use common::*;
use screen::Screen;
use shapes::Shapes;

use crate::shapes::Drawable;

mod common;
mod screen;
mod shapes;

fn main() {
    let mut screen = Screen::new(50, 50);
    let point = Shapes::Point(Coordinate { x: 20, y: 20 });
    let triangle = Shapes::Triangle(
        Coordinate { x: 25, y: 0 },
        Coordinate { x: 35, y: 40 },
        Coordinate { x: 10, y: 40 },
    );
    point.draw_on(&mut screen);
    triangle.draw_on(&mut screen);
    // screen.clear();
    let square = Shapes::Square {
        top_left: Coordinate { x: 5, y: 5 },
        len: 30,
    };
    square.draw_on(&mut screen);
    println!("{}", screen);
}
