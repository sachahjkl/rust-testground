use std::vec;

use crate::{
    common::{Coordinate, CoordinateOffset, Unit},
    screen::Screen,
};

const DRAWN_CHARACTER: char = '·';

pub trait Drawable {
    fn draw_on(&self, screen: &mut Screen);
    fn translate(&self, offset: CoordinateOffset);
}
#[derive(PartialEq, Eq, PartialOrd, Ord)]
pub enum Shapes {
    // Square {
    //     top_left: Coordinate,
    //     top_right: Coordinate,
    //     bottom_left: Coordinate,
    //     bottom_right: Coordinate,
    // },
    // Circle {
    //     center: Coordinate,
    //     radius: Unit,
    // },
    Polygon(Vec<Coordinate>),
    Triangle(Coordinate, Coordinate, Coordinate),
    Square { top_left: Coordinate, len: Unit },
    // Triangle(Coordinate, Coordinate, Coordinate),
    Point(Coordinate),
}

impl Drawable for Shapes {
    fn draw_on(&self, screen: &mut Screen) {
        match self {
            // Shapes::Square { top_left, top_right, bottom_left, bottom_right } => todo!(),
            // Shapes::Circle { center, radius } => todo!(),
            // Shapes::Triangle(_, _, _) => todo!(),
            Shapes::Point(coords) => {
                screen.blit(*coords, DRAWN_CHARACTER);
            }
            Shapes::Polygon(coords) => {
                assert!(coords.len() > 2, "Polygons consist of at least 3 vertices");
                let first = coords.first().unwrap();
                let last = coords.last().unwrap();
                screen.draw_line(*last, *first, DRAWN_CHARACTER);
                // screen.draw_line(*b, *c, DRAWN_CHARACTER);
                for window in coords.windows(2) {
                    screen.draw_line(window[0], window[1], DRAWN_CHARACTER)
                }
            }
            Shapes::Square { top_left, len } => Shapes::Polygon(vec![
                // Top left
                *top_left,
                // Top right
                Coordinate {
                    x: top_left.x + len,
                    y: top_left.y,
                },
                // Bottom right
                Coordinate {
                    x: top_left.x + len,
                    y: top_left.y + len,
                },
                // Bottom left
                Coordinate {
                    x: top_left.x,
                    y: top_left.y + len,
                },
            ])
            .draw_on(screen),
            Shapes::Triangle(a, b, c) => Shapes::Polygon(vec![*a, *b, *c]).draw_on(screen),
        }
    }

    fn translate(&self, _offset: CoordinateOffset) {
        todo!()
    }
}
