pub type Unit = i32;
pub type Line = Vec<char>;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Default, Debug)]
pub struct Coordinate {
    pub x: Unit,
    pub y: Unit,
}
#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Default, Debug)]
pub struct CoordinateOffset {
    pub dx: Unit,
    pub dy: Unit,
}
