use crate::{
    common::{Coordinate, Unit},
    Line,
};
use std::fmt::Display;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Default, Debug)]
pub struct Screen {
    lines: Vec<Line>,
}

impl Screen {
    pub fn blit(&mut self, coords: Coordinate, glyph: char) {
        match self.lines.get_mut(coords.y as usize) {
            Some(line) => match line.get_mut(coords.x as usize) {
                Some(cell) => {
                    *cell = glyph;
                }
                None => panic!("Out of bounds on x ! (max {})", line.len() - 1),
            },
            None => panic!(
                "Out of bounds y ! (max {})",
                self.lines.first().unwrap().len() - 1
            ),
        }
    }

    pub fn clear(&mut self) {
        for line in self.lines.iter_mut() {
            for cell in line.iter_mut() {
                *cell = ' ';
            }
        }
    }

    fn draw_line_high(&mut self, from: Coordinate, to: Coordinate, glyph: char) {
        let mut dx = to.x - from.x;
        let dy = to.y - from.y;
        let mut xi = 1;
        if dx < 0 {
            (xi, dx) = (-1, -dx)
        };
        let mut d = (2 * dx) - dy;
        let mut x = from.x;
        for y in from.y..to.y {
            self.blit(Coordinate { x, y }, glyph);
            if d > 0 {
                x = x + xi;
                d += 2 * (dx - dy);
            } else {
                d += 2 * dx
            }
        }
    }
    fn draw_line_low(&mut self, from: Coordinate, to: Coordinate, glyph: char) {
        let dx = to.x - from.x;
        let mut dy = to.y - from.y;
        let mut yi = 1;
        if dy < 0 {
            (yi, dy) = (-1, -dy)
        };
        let mut d = (2 * dy) - dx;
        let mut y = from.y;
        for x in from.x..to.x {
            self.blit(Coordinate { x, y }, glyph);
            if d > 0 {
                y = y + yi;
                d += 2 * (dy - dx);
            } else {
                d += 2 * dy
            }
        }
    }

    // Bresenham's line algorithm
    // plotLine(x0, y0, x1, y1)
    //  if abs(y1 - y0) < abs(x1 - x0)
    //      if x0 > x1
    //          plotLineLow(x1, y1, x0, y0)
    //      else
    //          plotLineLow(x0, y0, x1, y1)
    //      end if
    //  else
    //      if y0 > y1
    //          plotLineHigh(x1, y1, x0, y0)
    //      else
    //          plotLineHigh(x0, y0, x1, y1)
    //      end if
    //  end if
    pub fn draw_line(&mut self, from: Coordinate, to: Coordinate, glyph: char) {
        println!("drawing line for : {:?} {:?}", from, to);
        if (to.y - from.y).abs() < (to.x - from.x).abs() {
            if from.x > to.x {
                self.draw_line_low(to, from, glyph)
            } else {
                self.draw_line_low(from, to, glyph)
            }
        } else {
            if from.y > to.y {
                self.draw_line_high(to, from, glyph)
            } else {
                self.draw_line_high(from, to, glyph)
            }
        }
    }

    pub fn new(height: Unit, width: Unit) -> Screen {
        assert!(height > 1, "height must be bigger than 1");
        assert!(width > 1, "width must be bigger than 1");
        Screen {
            lines: vec![vec![' '; width as usize]; height as usize],
        }
    }
}

impl Display for Screen {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut render = String::from("");
        render.push_str(&"__".repeat(self.lines.first().unwrap().len() + 1));
        render.push_str("\n");
        for line in &self.lines {
            render.push_str("|");
            for (i, glyph) in line.iter().enumerate() {
                if i == line.len() - 1 {
                    render.push_str(&format!("{} |", glyph));
                } else {
                    render.push_str(&format!("{} ", glyph));
                }
            }
            render.push_str("\n");
        }
        render.push_str(&"‾‾".repeat(self.lines.first().unwrap().len() + 1));
        render.push_str("\n");
        return write!(f, "{}", render);
    }
}
